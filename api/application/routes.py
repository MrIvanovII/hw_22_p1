from flask import Blueprint, jsonify, request

from application.app import db
from application.database import User, Item
from application.controller import Controller

view = Blueprint("view", __name__)
controller = Controller(db)  

@view.route("/")
def home():
    """Домашняя страница"""
    lst = ''
    lst = "Home Work Part 1<br><br>"
    lst = lst + '<a href="/users">users</a><br>'
    lst = lst + '<a href="/items">items</a><br>'
    return lst

# Пользователь
@view.route("/registration", methods=["POST"])
def user_registration():
    """Регистрация пользователя"""

    return controller.add_user(request.json['login'],request.json['password'])

@view.route("/login", methods=["POST"])
def user_login():
    """Авторизация пользователя"""
    return controller.get_token(request.json['login'],request.json['password'])


# Объект
@view.route("/items/new", methods=["POST"])
def item_new():
    """Создание объекта"""
    return controller.add_item(request.json['token'],request.json['name'])

@view.route("/items/:id", methods=["DELETE"])
def item_delete():
    """Удаление объекта"""
    return controller.del_item(request.json['token'],request.json['id'])  

@view.route("/items/<token>")
def item_get_list(token: str):
    """Получение списка объектов по токену"""
    try:
        return jsonify([{'id':r[0],'name':r[1]} for r in controller.list_item(request.json['token'])])
    except Exception as ex:
        return str(ex)

# Передача объекта
@view.route("/send", methods=["POST"])
def item_send():
    """Генерация ссылки для передачи объекта"""
    try:
        data = controller.get_item(request.json['id'],request.json['login'],request.json['token'])
        if data:
            return jsonify({'ref':request.url_root+'get/'+str(data[0]),'token':data[1]})              
        else:
            return 'error'    
    except Exception as ex:
        return str(ex)

@view.route("/get")
def item_get(url: str, token: str):
    '''Переход по ссылке для получения объекта'''
    try:
        return None
    except Exception as ex:
        return str(ex)  


# Дополнительно
@view.route("/users")
def users_get():
    '''Получить список всех пользователей'''
    lst='<a href="/">HOME</a><br><br>'
    for user in User.query.all():
        lst = lst + f'{user.id} {user.login}<br>'
    return lst

@view.route("/items")
def items_get():
    '''Получить список всех объектов'''
    lst='<a href="/">HOME</a><br><br>'
    for item in Item.query.all():
        lst = lst + f'{item.id} {item.name} <br>'
    return lst