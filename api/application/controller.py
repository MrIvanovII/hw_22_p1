from application.app import db
from application.database import User, Item
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer


class Controller():
    def __init__(self, p_db) -> None:
        self.db = p_db
        self.token=Serializer('hw_22_p1',3600)

    # Пользователь
    def get_user(self,p_token:str):
        """Получение пользователя

        Args:
            p_token (str): Токен

        Returns:
            Any: Идентификатор пользователя
        """
        try:
           id_user=self.token.loads(p_token)['user_id'] 
           return id_user
        except Exception as ex:
            return None

    def get_token(self,p_login:str, p_password:str) -> str:
        """Получение токена

        Args:
            p_login (str): Логин
            p_password (str): Пароль

        Returns:
            str: Токен
        """
        try:
            rec= User.query.filter_by(login=p_login.upper()).first()
            if rec and rec.password==p_password: 
                return self.token.dumps({'user_id':rec.id}).decode('utf-8')
        except Exception as ex:
            return str(ex)

    def add_user(self, p_login:str, p_password:str) -> str:
        """Добавление пользователя

        Args:
            p_login (str): Логин
            p_password (str): Пароль

        Returns:
            str: Сообщение
        """
        try:
            rec = User.query.filter_by(login=p_login.upper()).first()
            if rec:
                return f"Пользователь {p_login} уже зарегистрирован"
            else:
                rec=User(p_login, p_password)                 
                self.db.session.add(rec)
                self.db.session.commit()
                return f'Пользователь {p_login} успешно зарегистрирован'
        except Exception as ex:
            self.db.session.rollback()             
            return str(ex)

    # Объект
    def add_item(self, p_token:str, p_name:str) -> str:
        """Добавление объекта

        Args:
            p_token (str): Токен
            p_name (str): Название объекта

        Returns:
            str:  Сообщение
        """
        try:
            id_user = self.get_user(p_token)
            if id_user:
                rec=Item(id_user, p_name)                 
                self.db.session.add(rec)
                self.db.session.commit()
                s = f'Объект {p_name} успешно зарегистрирован'
                s = s + f'\n {rec.id} {rec.name}'
                return s
        except Exception as ex:
            self.db.session.rollback()             
            return str(ex)


    def del_item(self, p_token:str, p_id:int) -> str:
        """Удаление объекта

        Args:
            p_token (str): Токен
            p_id (int): Идентификатор объекта

        Returns:
            str: Сообщение
        """
        try:
            id_user = self.get_user(p_token)
            rec = Item.query.filter_by(id = p_id).first()
            if id_user and rec and rec.user_id==id_user:
                self.db.session.delete(rec)
                self.db.session.commit()
                return f"Объект{rec.name} успешно удален"
        except Exception as ex:
            self.db.session.rollback()             
            return str(ex)

    def list_item(self, p_token:str) -> list:
        """Получение списка объектов

        Args:
            p_token (str): [description]

        Returns:
            list: Список объектов
        """
        try:
            id_user=self.get_user(p_token)
            if id_user:
                rec = Item.query.filter_by(user_id=id_user).all() 
                return [[r.id, r.name] for r in rec]    
        except Exception as ex:
            return None             

    def get_item(self, p_id:int, p_login:str, p_token:str) -> list:
        """Получение объекта

        Args:
            p_id (int): Идентификатор объекта
            p_login (str): Логин принимающего пользователя
            p_token (str): Токен

        Returns:
            list: Объект
        """
        user_id=self.get_user(p_token)
        try:
            rec= User.query.filter_by(login=p_login.upper()).first()
            if rec and user_id and p_id:
                r = Item.query.filter_by(id=p_id).first() 
                if r:
                    self.token=Serializer('hw_22_p1',3600)
                    return [r.id, self.token.dumps({'user_id':rec.id}).decode('utf-8')]
        except Exception as ex:
            return None 

    def move_item(self, id_item, p_token:str) -> str:
        """Перемещение объекта

        Args:
            id_item ([type]): Идентификатор объекта
            p_token (str): Токен

        Returns:
            str: Сообщение
        """
        try:
            if self.token: 
                id_user=self.token.loads(p_token)['user_id'] 
            if id_item and id_user:
                rec = Item.query.filter_by(id=id_item).first() 
            if rec:
                rec.user_id=id_user
                self.db.session.commit()
                return f"Объект успешно перемещен"   
        except Exception as ex:
            return str(ex) 
 
