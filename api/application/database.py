from application.app import db


class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(20),nullable=False,unique=True)
    password = db.Column(db.String(20),nullable=False)

    def __init__(self, p_login:str, p_password:str) -> None:
        self.login = p_login.upper()
        self.password = p_password

    def __str__(self) -> str:
        return f"{self.id}"

class Item(db.Model):
    __tablename__ = "items"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer,db.ForeignKey('users.id'),nullable=False)
    name = db.Column(db.String(200),nullable=False)

    def __init__(self, p_user_id:int, p_name:str) -> None:
        self.user_id = p_user_id
        self.name = p_name

    def __str__(self) -> str:
        return f"{self.id}"