import pytest
from application.app import app, db
from application.database import User, Item


@pytest.fixture
def client():
    yield app.test_client()
    truncate_DB()


def truncate_DB():
    with app.app_context():
        User.query.delete()
        Item.query.delete()
        db.session.commit()



if __name__ == "__main__":
    pytest.main()
